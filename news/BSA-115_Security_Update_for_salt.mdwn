[[!meta date="2017-06-26 22:16:01 UTC"]]
	Al Nikolov uploaded new package for salt which fixed the
	following security problem:
	
	CVE-2017-8109
	    The salt-ssh minion code in SaltStack Salt 2016.11 before 2016.11.4
	    copied over configuration from the Salt Master without adjusting
	    permissions, which might leak credentials to local attackers on
	    configured minions (clients).
	
	For the jessie-backports distribution the problems have been fixed in
	version 2016.11.2+ds-1~bpo8+2.
